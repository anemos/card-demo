export * from "./Slider/Slider";
export * from "./ProductInfo/ProductInfo";
export * from "./App";
export * from "./ProductInfo/Breadcrumbs/Breadcrumbs";
export * from "./ProductInfo/DetailInfo/DetailInfo";
export * from "./Recomendation/Recomendation";
